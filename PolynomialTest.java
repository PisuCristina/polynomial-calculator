import model.Polynomial;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
class PolynomialTest {

    Polynomial p1 = new Polynomial();
    Polynomial p2 = new Polynomial();
    Polynomial p3 = new Polynomial();

    @BeforeEach
    void setUp() {
      p1.createPolinom("x^5+2x^4+4x");
      p2.createPolinom("2x^4+1");
      p3.createPolinom("0");
    }

    @Test
    @DisplayName("Polynomial simple addition")
    void testSimpleAddition() {
        p1.add(p2);
        assertEquals("x^5+4x^4+4x+1",  p1.getPolinomString(),
                "Regular addition  works");
    }

   @Test
    @DisplayName("Addition with 0")
    void additionWithZero() {
        p1.add(p3);
        assertEquals("x^5+2x^4+4x",  p1.getPolinomString(),
                "Multiplication with 0 works");
    }

  @Test
    @DisplayName("Simple multiplication should work")
    void simpleMultiplication() {
        p1.mutiply(p2);
        p1.sortMonomsIntoPolynomial();
        assertEquals("2x^9+4x^8+x^5+8x^5+2x^4+4x", p1.getPolinomString(),
                "Regular multiplication of polynomials works");
    }

    @Test
    @DisplayName("Subtraction should work")
    void subtraction() {
        p1.subtract(p2);
        assertEquals("x^5+4x-1", p1.getPolinomString(),
                "Subtraction on polynomials works");
    }

 @Test
    @DisplayName("Multiplication with zero")
    void multiplicationWithZero() {
        p1.mutiply(p3);
        assertEquals("",  p1.getPolinomString(),
                "Multiplication with zero works");
    }

    @Test
    @DisplayName("Differentiate polynomial")
    void differentiatePolynomial() {
        p1.derivatePolinom();
        assertEquals("5x^4+8x^3+4",  p1.getPolinomString(),
                "Differentiating operation works");
    }

    @Test
    @DisplayName("Integrate polynomial")
    void integratePolynomial() {
        p1.intergatedPolinom();
        assertEquals("0.16666666666666666x^6+0.4x^5+2.0x^2",  p1.getIntegrationPolinomString(),
                "Integration operation works");
    }

}
